# Exports 

# General
export WM="bspwm"
export TERMINAL="alacritty"
export BROWSER="brave"
export FILE="pcmanfm"
export EDITOR="nvim"
export XDG_CONFIG_HOME="$HOME/.config"
export ZDOT="/home/audioz/.config/zsh/"
export PATH=$PATH:~/.local/bin

# Java
export JDTLS_HOME="/usr/share/java/jdtls"
export _JAVA_AWT_WM_NONREPARENTING=1

/* min (vertical) line thickness */
#define MIN_THICKNESS 1

/* max (vertical) line thickness */
#define MAX_THICKNESS 6

/* base color to use, distance from center will multiply the RGB components */
#define BASE_COLOR vec4(1.000,0.843,0.0, 1)

/* amplitude */
#define AMPLIFY 500

/* outline color */
#define OUTLINE vec4(0.345,0.345,0.345, 1)
